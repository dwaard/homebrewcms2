<?php
/**
 * Article.php - renders articles, either to show or to manipulate them
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';

// Read qyeryvars or make defaults
$mode = 'show';
if (isset ( $_GET ['mode'] ))
	$mode = $_GET ['mode'];

$id = 0;
if (isset ( $_GET ['id'] ))
	$id = $_GET ['id'];
	
// Read the article from the database
$article = null;
global $mysqli;
$sql = "SELECT * FROM ARTICLE WHERE ID=" . $id . ";";
$result = $mysqli->query ( $sql );
if ($result)
	$article = $result->fetch_assoc ();

if ($mode == 'show' || $mode == 'delete_confirm') {
	if ($mode === 'delete_confirm') {
		echo 'Artikel ' . $id . ' verwijderen?<br/>';
		echo '<a class="btn btn-danger" href="?action=delete&page=article&id=' . $id . '">OK</a>';
		echo '&nbsp&nbsp';
		echo '<a class="btn btn-default" href="?action=show&page=article&id=' . $id . '">Annuleren</a>';
	}
	echo '<p/>';
	echo '<h1>' . $article ['Name'] . '</h1>';
	echo $article ['Content'];
} else {
	$isNew = $id == 0;
	
	echo '<form action="?action=save&page=article&id=' . $article ['ID'] . '" method="post" id="articleform">';
	if (!$isNew) {
		echo '<input type="hidden" name="ID" value="' . $article ['ID'] . '"/>';
	}
	echo '<div class="form-group">';
	echo '<label class="control-label col-sm-2" for="Name">Titel van het artikel: </label>';
	echo '<div class="col-sm-10">';
	echo '<input class="form-control" type="text" name="Name"';
	if (! $isNew) {
		echo ' value="' . $article ['Name'] . '"';
	}
	echo '></input>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group">';
	echo '<label class="control-label col-sm-12" for="Name">Inhoud van het artikel:</label>';
	echo '<div class="col-sm-12">';
	echo '<textarea class="form-control" rows="34" name="Content">';
	if (! $isNew)
		echo $article ['Content'];
	echo '</textarea>';
	echo '</div>';
	echo '</div>';
	echo '<div class="form-group">';
	echo '<div class="col-sm-12">';
	echo '<button type="submit" class="btn btn-default">Opslaan</button>';
	echo '</div>';
	echo '</div>';
	echo '</form>';
}
?>